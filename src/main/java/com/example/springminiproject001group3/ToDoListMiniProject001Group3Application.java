package com.example.springminiproject001group3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDoListMiniProject001Group3Application {

    public static void main(String[] args) {
        SpringApplication.run(ToDoListMiniProject001Group3Application.class, args);
    }

}
